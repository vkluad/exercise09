# Exercise09
## Subtask #1 Build and test ex01 module:
- Copy the following code `ex01.c`
```c
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("xone");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

static int __init ex01_init(void)
{
    printk(KERN_INFO "Hello!!!\n");
    return 0;
}

static void __exit ex01_exit(void)
{
    printk(KERN_INFO "Bye...\n");
}

module_init(ex01_init);
module_exit(ex01_exit);
```
- and `Makefile`
```
obj-m += ex01.o
all:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
test:
    sudo dmesg -C
    sudo insmod ex01.ko
    sudo rmmod ex01.ko
    sudo dmesg
```
run command `make all`
run command `make test`
result after command file ex01.ko:
```
ex01.ko: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), BuildID[sha1]=b308e512c269810e8ee9fdcc65af749939d4a826, with debug_info, not stripped
```

## Subtask #2 Fix bugs, build and test modules ex02 and test_ex02:

- Copy the following code `ex02.c`:
```c
#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("xone");
MODULE_VERSION("0.01");
MODULE_DESCRIPTION("A simple example Linux module.");

static char retpref[] = "this string returned from ";

char *test_01(void)
{
    static char res[80];
    strcpy(res, retpref);
    strcat(res, __FUNCTION__);
    return res;
}
EXPORT_SYMBOL(test_01);

char *test_02(void)
{
    static char res[80];
    strcpy(res, retpref);
    strcat(res, __FUNCTION__);
    return res;
}
EXPORT_SYMBOL(test_02);

static int __init xinit(void)
{
    printk(KERN_INFO "ex02: Hello!!!\n");
    printk(KERN_INFO "ex02: %s\n", test_01());
    printk(KERN_INFO "ex02: %s\n", test_02());
    return 0;
}

static void __exit xexit(void)
{
    printk(KERN_INFO "ex02: Bye...\n");
}
```
- and `test_ex02`:
```c
#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("xone");
MODULE_VERSION("0.01");
MODULE_DESCRIPTION("Test ex02 Linux module.");

extern char *test_01(void);
extern char *test_02(void);

static int __init xinit(void)
{
    printk(KERN_INFO "test_ex02: Hello!!!\n");
    printk(KERN_INFO "test_ex02: %s\n", test_01());
    printk(KERN_INFO "test_ex02: %s\n", test_02());
    printk(KERN_INFO "test_ex02: Bye...\n");
    return 0;
}
```
- and `Makefile`:

```
obj-m += ex02.o test_ex02.o
all:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
test:
    # Add your implementation here
```

- For fix the next bugs you must add:
```c
...

module_init(xinit);
module_exit(xexit);
```
- in the end of file `ex02.c` and add:
```c
...
module_init(xinit);
```
- in the end of file `test_ex02.c`.


- For `make test` you must add some implementation for target test for example:
```
test:
	sudo dmesg -C
        sudo insmod ex02.ko
        - sudo insmod test_ex02.ko
        sudo lsmod | head -n 5
        sudo rmmod ex02
        sudo lsmod | head -n 5
        sudo dmesg
```
- Run it( `make && make test`)
- You can see next:
```
sudo dmesg -C
sudo insmod ex02.ko
sudo insmod test_ex02.ko
insmod: ERROR: could not insert module test_ex02.ko: Operation not permitted
make: [Makefile:9: test] Error 1 (ignored)

sudo lsmod | head -n 5
Module                  Size  Used by
ex02                   16384  0
ccm                    20480  6
snd_sof_pci_intel_cnl    16384  0
snd_sof_intel_hda_common   106496  1 snd_sof_pci_intel_cnl
sudo rmmod ex02

sudo lsmod | head -n 5
Module                  Size  Used by
ccm                    20480  6
snd_sof_pci_intel_cnl    16384  0
snd_sof_intel_hda_common   106496  1 snd_sof_pci_intel_cnl
soundwire_intel        45056  1 snd_sof_intel_hda_common
sudo dmesg
[13517.602656] ex02: Hello!!!
[13517.602658] ex02: this string returned from test_01
[13517.602659] ex02: this string returned from test_02
[13517.612322] test_ex02: Hello!!!
[13517.612324] test_ex02: this string returned from test_01
[13517.612325] test_ex02: this string returned from test_02
[13517.612325] test_ex02: Bye...
[13517.752639] ex02: Bye...
```

## Build the previously created kernel modules for Linux deployed with Qemu. Install and remove them
- Edit Makefile or create new for complate this subtask:
```
obj-m += ex02.o test_ex02.o
all:
        make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
        make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
all_i386:
	make -C ${BUILD_KERNEL} M=$(PWD) modules
clean_i386:
	make -C ${BUILD_KERNEL} M=$(PWD) clean
all_rpi:
	make -C ${BUILD_KERNEL_ARM} M=$(PWD) modules
clean_rpi:
	make -C ${BUILD_KERNEL_ARM} M=$(PWD) clean
test:
        sudo dmesg -C
        sudo insmod ex02.ko
        - sudo insmod test_ex02.ko
        sudo lsmod | head -n 5
        sudo rmmod ex02
        sudo lsmod | head -n 5
        sudo dmesg
```
> `BUILD_KERNEL` and `BUILD_KERNEL_ARM` : path from previos exercise and you can run `initial_script` from [here](https://gitlab.com/vkluad/exercise08/-/raw/master/initial_script).

- For copy module to qemu use scp `scp -P 8022 $(PWD) root@localhost:` use this command in folder with exercise
	- after in qemu run `./insrem`

Result:

```
Module                  Size  Used by
ex02			16384 0	
Module                  Size  Used by
[407.622266] ex02: Hello!!!
[407.622386] ex02: this string returned from test_01
[407.622451] ex02: this string returned from test_02
[407.701395] test_ex02: Hello!!!
[407.701489] test_ex02: this string returned from test_01
[407.701544] test_ex02: this string returned from test_02
[407.701589] test_ex02: Bye...
[407.829153] ex02: Bye...
```

`file ex02.ko`:

```
ex02.ko: ELF 32-bit LSB relocatable, Intel 80386, version 1 (SYSV), BuildID[sha1]=6feab55d82ab00564694549fab370fbcc1a0f10e, not stripped
```
